package week3.practicum;

import library.TextIO;

public class Password {

	public static void main(String[] args) {

		String secret;

		System.out.println("You need a password. Imput one");
		String password = TextIO.getlnString();
		
		while (true) {

			System.out.println("Log in with your password");
			secret = TextIO.getlnString();

			if (!(password.equals(secret))) {

				System.out.println("INCORRECT PASSWORD, PLEASE RETRY");

			} else {
				System.out.println("Correct password!");
				break;
			}
		}

		
		
	}

}
