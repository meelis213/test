package week3.practicum;

import library.TextIO;

public class Grades {

	public static void main(String[] args) {

		System.out.println("Give me your grade!");
		double grade = TextIO.getlnDouble();
		if (grade > 5) {
			System.out.println("Your grade is too high!");

		} else if (grade < 4.5) {
			System.out.println("Your grade is too low!");
		} else if (grade < 0) {
			System.out.println("Your grade is negative");
		} else {
			System.out.println("Now give me your thesis grade!");
			double thesis = TextIO.getlnDouble();
			if (thesis != 5) {
				System.out.println("No Cum Laude for you!");
			} else {
				System.out.println("Cum Laude for you!");

			}
		}

	}
}
