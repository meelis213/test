package week3.practicum;

import library.TextIO;

public class OddEven {

	public static void main(String[] args) {

		int number;
		while (true) {

			System.out.println("Give me a number!");
			number = TextIO.getlnInt();

			if (number < 0) {
				System.out.println("Give me a real number");

			} else
				break;

		}
		if ((number % 2) == 0) {

			System.out.println("Your number is even!");

		} else

		{
			System.out.println("Your number is odd!");

		}
	}
}
