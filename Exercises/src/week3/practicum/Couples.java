package week3.practicum;

import library.TextIO;

public class Couples {

	public static void main(String[] args) {

		int ageDiff;

		while (true) {
			System.out.println("Give me an age");
			int age1 = TextIO.getlnInt();
			System.out.println("Now give me another age");
			int age2 = TextIO.getlnInt();
			ageDiff = Math.abs(age1 - age2);

			if (age1 > 0 && age2 > 0) {
				break;
			} else
				System.out.println("Fake ages, try again");
		}

		if (ageDiff >= 5 && ageDiff <= 10) {
			System.out.println("Nice!");
		} else if (ageDiff >= 11 && ageDiff <= 15) {
			System.out.println("Not that nice");
		} else if (ageDiff >= 15) {
			System.out.println("Not okay");
		} else if (ageDiff < 5) {
			System.out.println("Very nice");
		}

	}

}
