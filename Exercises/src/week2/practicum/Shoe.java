package week2.practicum;
import library.TextIO;

public class Shoe {
	public static void main(String[] args) {
		System.out.println("Hello! Please tell me your name!");
		String name = TextIO.getlnString();
		System.out.println("Okay. Now tell me your shoe size");
		Integer shoe = TextIO.getInt();
		
		System.out.println("You are " + name + " and your shoe size is " + shoe);
	}

}
