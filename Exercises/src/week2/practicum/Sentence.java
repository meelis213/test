package week2.practicum;

import library.TextIO;

public class Sentence {

	public static void main(String[] args) {
		
		System.out.println("Give me a sentence!");
		String sentence = TextIO.getlnString();
		System.out.println(sentence.replace('a', '_').replace('A', '_'));
		
	}
	
}
