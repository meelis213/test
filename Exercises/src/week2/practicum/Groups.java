package week2.practicum;

import library.TextIO;

public class Groups {

	public static void main(String[] args) {

		System.out.println("Give me a number of people!");
		int people = TextIO.getlnInt();
		System.out.println("Now give me a number of groups");
		int groups = TextIO.getlnInt();

		int answer = people % groups;

		System.out.println("The amount of people being left out is " + answer);
	}

}
