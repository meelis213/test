package week2.practicum;

import library.TextIO;

public class Numbers {

	public static void main(String[] args) {

		System.out.println("Give me a number!");
		int first = TextIO.getlnInt();
		System.out.println("Give me another number!");
		int second = TextIO.getlnInt();

		System.out.println(first * second);
	}
}
